#ifndef POPUPWIN_HPP
#define POPUPWIN_HPP

#include <QDialog>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QLabel>

#define POPUP_WIDTH 270
#define POPUP_HEIGHT 100
#define MAX_FILE_NAME 30

class PopUpWin : public QDialog
{
    Q_OBJECT

    private:
        QPushButton *confirm;
        QPushButton *cancel;
        QLineEdit *file_insert;
        QLabel *edit_name;
        QVBoxLayout *vert_layout;
        QHBoxLayout *line_edit_layout;
        QHBoxLayout *buttons_layout;

        void setInnerWidgets();
        void setInnerLayouts();

    public:
        PopUpWin(const char *popup_title, QWidget *parent = nullptr);

    public slots:
        void takeFilename();
        void quit();

    signals:
        void recordFileName(QString filename);
};

#endif // POPUPWIN_HPP
