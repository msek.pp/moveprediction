#ifndef RINGBUFFER_HPP
#define RINGBUFFER_HPP

#include <iostream>

/*!
 * \brief The RingBuffer class
 *
 * Class that defines circular (ring) buffer data structure.
 * It's using C++ templates mechanism to implement any data type.
 */

#define MAX_BUFF_SIZE 100000L

template <typename AnyType>
class RingBuffer
{
    private:
        AnyType *ring_array;
        uint16_t current_index = 0;
        uint16_t size = 0;

    public:
        /*!
         * \brief 'RingBuffer' constructor
         */
        RingBuffer(uint16_t buff_size = 1000);
        /*!
         * \brief 'RingBuffer' destructor
         */
        ~RingBuffer();

        AnyType & operator[] (uint16_t index);

        void addNewValue(AnyType new_value);
        void displayContent();

        uint16_t retBufferSize() { return this->size; }
        uint16_t retBufferSize() const { return this->size; }
        AnyType retLastAdded() { return this->ring_array[this->current_index - 1]; }
};

#endif // RINGBUFFER_HPP
