#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QTabWidget>
#include "RealTimeDisp.hpp"
#include "SampleMaker.hpp"

#define WIDTH 1000
#define HEIGHT 800

class MainWindow : public QMainWindow
{
    Q_OBJECT

    private:
        QTabWidget *windows;
        RealTimeDisp *rt_data_viewer;
        SampleMaker *sample_maker;
        QWidget *main_widget;
        QHBoxLayout *main_layout;

    public:
        MainWindow(QWidget *parent = nullptr, unsigned int x = WIDTH, unsigned int y = HEIGHT);
        ~MainWindow();

        SerialPort *retSerialHandler() { return this->rt_data_viewer->retSerialHandler(); }
};

#endif // MAINWINDOW_HPP
