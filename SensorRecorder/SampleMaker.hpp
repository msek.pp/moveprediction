#ifndef SAMPLEMAKER_HPP
#define SAMPLEMAKER_HPP

#include <iostream>
#include <fstream>
#include <QPushButton>
#include "SignalViewer.hpp"
#include "StaticDataViewer.hpp"
#include "InfoEdit.hpp"
#include "PopUpWin.hpp"

#define CLASSES_NUM 6

static std::string signal_classes[CLASSES_NUM] = {"open_hand", "thumb_up", "two", "three", "four", "fist"};

class SampleMaker : public SignalViewer
{
    Q_OBJECT
    private:
        QPushButton *load_record;
        QPushButton *save_sample;
        InfoEdit *window_size;
        InfoEdit *data_label;
        PopUpWin *import_popup;
        Filters *smoother;

        uint16_t file_counter[CLASSES_NUM];

        float *recorded_signal = nullptr;
        float *filtered_signal = nullptr;

        uint16_t data_length = 0;
        uint16_t win_start_index = 0;
        uint16_t win_size = 0;

        void setInnerLayouts() override;
        void setInnerWidgets() override;

    public:
        SampleMaker(QWidget *parent = nullptr);
        ~SampleMaker();

    public slots:
        void drawWindow();
        void openImportPopup();
        void importRecord(QString filename);
        void saveCuttedSample();
        void keyPressEvent(QKeyEvent *event) override;
};

#endif // SAMPLEMAKER_HPP
