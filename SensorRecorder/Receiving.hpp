#ifndef RECEIVING_HH
#define RECEIVING_HH

#include <string>
#include "Transparam.hpp"

bool RS232_Odbierz(int            DeskPortu,
                   std::string   &Bufor,
                   unsigned int   IloscZnakow,
                   unsigned int   CzasOczekiwania_ms = 2000);

#endif // RECEIVING_HH
