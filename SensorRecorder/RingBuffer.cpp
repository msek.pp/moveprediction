#include "RingBuffer.hpp"

/*!
 * \brief RingBuffer class constructor
 *
 * Constructs new object of class 'RingBuffer' with given data type and
 * array size.
 *
 * \param [arg] buff_size - size of the buffer / array (default is 1000 elements)
 */
template <typename AnyType>
RingBuffer<AnyType>::RingBuffer(uint16_t buff_size)
{
    this->ring_array = new AnyType[buff_size];
}

template <typename AnyType>
void RingBuffer<AnyType>::addNewValue(AnyType new_value)
{
    // Add new value at current index
    this->ring_array[this->current_index] = new_value;
    // Increment current index
    this->current_index++;
    // Check if its the end of an array. If so, go to the beginning
    if((this->current_index != 0) && (this->current_index % this->size == 0))
        this->current_index = 0;
}

template <typename AnyType>
AnyType & RingBuffer<AnyType>::operator [] (uint16_t index)
{
    if(index >= this->size)
    {

    }
}

/*!
 * \brief
 */
template <typename AnyType>
void RingBuffer<AnyType>::displayContent()
{
    // Display overall informations about buffer
    std::cout << "\nBuffer size and current index (size, index): "
              << "(" << this->size << ", " << (this->current_index - 1) << ")\n";
    // Display content of the buffer
    std::cout << "\nBuffer content: [";

    for(int i = 0; i < this->size; ++i)
    {
        // Add new line after displaing 10 values
        if((i != 0) && (i % 10 == 0))
            std::cout << "\n";

        std::cout << this->ring_array[i] << " ";
    }
    // End displayed content with square bracket
    std::cout << "]\n";
}

template <typename AnyType>
RingBuffer<AnyType>::~RingBuffer()
{
    delete [] this->ring_array;
}
