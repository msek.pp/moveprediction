#ifndef REALTIMEDISP_HPP
#define REALTIMEDISP_HPP

#include <fstream>
#include <QPushButton>
#include <QTimer>
#include "SignalViewer.hpp"
#include "TimeDataViewer.hpp"
#include "SerialPort.hpp"
#include "PopUpWin.hpp"

// Time interval for checking serial port (which sends data each 20ms)
#define TIME_INTERVAL 5
// For efficiency, data will be recorded into static array with
// space for almost 3 minutes and 40 seconds of signal, which is 100 000 values
#define SIGNAL_MAX_LENGTH 100000

class RealTimeDisp : public SignalViewer
{
    Q_OBJECT

    private:
        QPushButton *record_data;
        QPushButton *clear_plot;
        QPushButton *start;
        SerialPort *serial;
        PopUpWin *save_popup;
        QTimer *serial_timer;
        Filters *smoother;

        bool recording = false;
        bool connected = false;
        // Array with data recorded
        float recorded_data[SIGNAL_MAX_LENGTH];
        // Array with real data
        uint16_t raw_signal[SIGNAL_MAX_LENGTH];
        // Array with filtered data
        uint16_t filtered_signal[SIGNAL_MAX_LENGTH];
        // Index for recorded data array
        uint16_t record_index = 0;
        // Index for raw signal
        uint16_t raw_index = 0;
        // Index for filtering data
        uint16_t filtered_index = 0;

        void setInnerWidgets() override;
        void setInnerLayouts() override;
        void startRecording();
        void stopRecording();

    public:
        RealTimeDisp(QWidget *parent = nullptr);
        ~RealTimeDisp() { }

        SerialPort *retSerialHandler() { return this->serial; }

    public slots:
        void toggleRecord();
        void toggleConnect();
        void clearPlot();
        void checkSerial();
        void connectSerialPlot();
        void disconnectSerialPort();
        void saveRecord(QString filename);
};

#endif // REALTIMEDISP_HPP
