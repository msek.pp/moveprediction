#ifndef DATAVIEWER_HPP
#define DATAVIEWER_HPP

#include "qcustomplot.hpp"
#include "FramedWidget.hpp"

#define PLOT_MIN_HEIGHT 200

/*!
 * \brief The DataViewer class
 *
 * Parent class for two specific plot in application design:
 *     - real time data viewer (data from sensor)
 *     - static data viewer (data already recorded)
 */
class DataViewer : public FramedWidget
{
    protected:
        QCustomPlot *plot;
        void setPlotGradient(QGradient new_gradient);

    private:
        void setVerticalLayout() override;

    public:
        DataViewer(const char *plot_title, QWidget *parent = nullptr);
        virtual ~DataViewer() { }

        virtual void addNewValue(int new_value, int time_interval) = 0;
        virtual void addNewValue(float new_value, int time_interval, int plot_num = 0) = 0;
        virtual void resetGraph() = 0;
        virtual void placeWindow(int start, int win_size) = 0;
        virtual void moveWindow(int start, int win_size) = 0;

        void fitAxesToData();
        void replot() { this->plot->replot(); }
};

#endif // DATAVIEWER_HPP
