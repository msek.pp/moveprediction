#ifndef FILTERS_HPP
#define FILTERS_HPP

#include <QDebug>
#include <bits/stdc++.h>
#include <cmath>
#include <math.h>
#include <algorithm>

class Filters
{
    private:
        uint8_t window_size = 0;
        double sigma = 0;
        uint16_t last_filtered = 0;

        double *gauss_weights;
        uint16_t *triangle_weights;
        uint16_t triangle_weights_sum = 0;

        void countGaussWeights();
        void countTriangleWeights();

    public:
        Filters(uint8_t window = 11, double gauss_spread = 6);
        ~Filters();

        uint16_t MedianFilter(uint16_t *input_signal, uint16_t start);
        uint16_t AverageFilter(uint16_t *input_signal, uint16_t start);
        uint16_t GaussianAverageFilter(uint16_t *input_signal, uint16_t start);
        uint16_t TriangleAverageFilter(uint16_t *input_signal, uint16_t start);
        float TriangleAverageFilter(float *input_signal, uint16_t start);
        uint16_t ExponentialAverageFilter(uint16_t old_info, uint16_t new_info);
};

#endif // FILTERS_HPP
