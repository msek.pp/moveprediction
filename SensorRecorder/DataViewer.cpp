#include "DataViewer.hpp"

DataViewer::DataViewer(const char *plot_title, QWidget *parent) : FramedWidget(plot_title, parent)
{
    this->plot = new QCustomPlot();

    setMinimumHeight(PLOT_MIN_HEIGHT);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

    setVerticalLayout();
}

void DataViewer::setVerticalLayout()
{
    v_layout->addWidget(title);
    v_layout->addWidget(plot);
}

void DataViewer::setPlotGradient(QGradient new_gradient)
{
    plot->setBackground(new_gradient);
}

void DataViewer::fitAxesToData()
{
    this->plot->rescaleAxes();
}
