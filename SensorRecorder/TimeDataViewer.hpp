#ifndef TIMEDATAVIEWER_HPP
#define TIMEDATAVIEWER_HPP

#include "DataViewer.hpp"

#include <iostream>

class TimeDataViewer : public DataViewer
{
    Q_OBJECT
    private:
        double time_elapsed = 0;
        double replot_time = 0;
        void setStoredWidgets() override;
        void updateTimeElapsed(int time_interval_ms);
        void checkReplot();

    public:
        TimeDataViewer(const char *plot_title, QWidget *parent = nullptr);
        void addNewValue(int new_value, int time_interval) override;
        void addNewValue(float new_value, int time_interval, int plot_num = 0) override;
        void resetGraph() override;
        void placeWindow(int start, int win_size) override;
        void moveWindow(int start, int win_size) override;
};

#endif // TIMEDATAVIEWER_HPP
