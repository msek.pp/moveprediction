#include "RealTimeDisp.hpp"

RealTimeDisp::RealTimeDisp(QWidget *parent) : SignalViewer(parent)
{
    this->plot = new TimeDataViewer("EMG signal", this);
    this->record_data = new QPushButton(tr("Record"), this);
    this->clear_plot = new QPushButton(tr("Clear plot"), this);
    this->start = new QPushButton(tr("Connect"), this);
    this->save_popup = new PopUpWin("Save record", this);
    this->serial = new SerialPort();
    this->serial_timer = new QTimer();

    this->smoother = new Filters(MOVING_WIN);
    // Fill first half window part of an filtered signal array with zeros
    for(int i = 0; i < (MOVING_WIN / 2) + 1; ++i)
        this->filtered_signal[i] = 0;
    this->filtered_index = MOVING_WIN / 2;

    this->setInnerWidgets();
    this->setInnerLayouts();

    // Connect timer's timeout with checking if new data were readed
    connect(this->serial_timer, SIGNAL(timeout()), this, SLOT(checkSerial()));
    // Connect pressing 'record' button with toggling record (start / stop)
    connect(this->record_data, SIGNAL(pressed()), this, SLOT(toggleRecord()));
    // Connect pressing 'start' button with starting reading from serial
    connect(this->start, SIGNAL(pressed()), this, SLOT(toggleConnect()));
    // Connect pressing 'clear_plot' button with clearing plot
    connect(this->clear_plot, SIGNAL(pressed()), this, SLOT(clearPlot()));
    // Connect closing pop up window with saving record
    connect(this->save_popup, SIGNAL(recordFileName(QString)), this, SLOT(saveRecord(QString)));

    *this->terminal << "Signals connected\n\n";
}

void RealTimeDisp::setInnerWidgets()
{
    this->setButtonProp(this->record_data);
    this->setButtonProp(this->clear_plot);
    this->setButtonProp(this->start);

    this->setButtonColor(this->record_data, QColor(GREY));
    this->setButtonColor(this->clear_plot, QColor(NEUTRAL));
    this->setButtonColor(this->start, QColor(GREEN));

    this->serial_timer->setInterval(TIME_INTERVAL);
    this->serial_timer->setSingleShot(false);
}

void RealTimeDisp::setInnerLayouts()
{
    this->buttons_layout->addWidget(this->record_data);
    this->buttons_layout->addItem(new QSpacerItem(10, 10, QSizePolicy::Minimum, QSizePolicy::Preferred));
    this->buttons_layout->addWidget(this->clear_plot);
    this->buttons_layout->addItem(new QSpacerItem(10, 10, QSizePolicy::Minimum, QSizePolicy::Preferred));
    this->buttons_layout->addWidget(this->start);

    this->hor_layout->addLayout(this->buttons_layout);
    this->hor_layout->addWidget(this->terminal);

    this->main_layout->addWidget(this->plot);
    this->main_layout->addItem(new QSpacerItem(30, 30, QSizePolicy::Minimum, QSizePolicy::Preferred));
    this->main_layout->addLayout(this->hor_layout);;
}

void RealTimeDisp::toggleRecord()
{
    if(!this->connected)
        return;
    // Start recording
    if(!this->recording)
        this->startRecording();
    // End recording
    else
        this->stopRecording();
}

void RealTimeDisp::clearPlot()
{
    this->plot->resetGraph();
}

void RealTimeDisp::startRecording()
{
    // Start recording
    this->recording = true;
    // Edit button
    this->record_data->setText(tr("Stop"));

    this->setButtonColor(this->record_data, QColor(RED));
}

void RealTimeDisp::stopRecording()
{
    // Stop recording
    this->recording = false;
    // Edit button
    this->record_data->setText(tr("Record"));

    if(this->connected)
        this->setButtonColor(this->record_data, QColor(GREEN));
    else
        this->setButtonColor(this->record_data, QColor(GREY));
    // Open window and save record to file
    this->save_popup->open();
}

// Called whenever timer counts to zero
void RealTimeDisp::checkSerial()
{
    bool overflow_protection = false;
    // Another way to acquire data
    if(!this->serial->receiveData())
        return;

    // Load new values to plot and buffer if recording is on
    uint16_t *buffer = this->serial->retEMGData();

    for(int i = 0; i < FRAME_LENGTH; ++i)
    {
        if(this->recording && !overflow_protection)
        {
            this->recorded_data[this->record_index] = (double)(this->filtered_signal[this->filtered_index - 1]) / 100.0;
            this->record_index++;

            if(this->record_index >= SIGNAL_MAX_LENGTH)
                overflow_protection = true;
        }
        // ~~~~~~~~ Here store and filter data ~~~~~~~~
        // Add raw data to array
        this->raw_signal[this->raw_index] = buffer[i];
        this->raw_index++;
        // Filter signal
        if(this->raw_index >= MOVING_WIN)
        {
            //this->filtered_signal[this->filtered_index] = this->smoother->TriangleAverageFilter(this->raw_signal, this->raw_index - MOVING_WIN);
            this->filtered_signal[this->filtered_index] = this->smoother->MedianFilter(this->raw_signal, this->raw_index - MOVING_WIN);
            this->plot->addNewValue((float)(this->filtered_signal[this->filtered_index]/100.0), PLOT_TIME * (MOVING_WIN / 2 + 1), 1);
            //this->plot->addNewValue((float)(this->filtered_signal[this->filtered_index]/100.0), PLOT_TIME);
            this->filtered_index++;
        }
        this->plot->addNewValue((float)(buffer[i]/100.0), PLOT_TIME);
    }

    if(overflow_protection)
    {
        this->stopRecording();
        this->terminal->typeOverflowError();
    }
}

void RealTimeDisp::toggleConnect()
{
    // Connect when disconnected
    if(!this->connected)
        connectSerialPlot();
    else
        disconnectSerialPort();
}

void RealTimeDisp::connectSerialPlot()
{
    if(this->serial->startConnection())
    {
        this->terminal->typeStartCommunication();
        this->connected = true;
        this->serial_timer->start();
        // Change button visuals for disconnecting
        this->start->setText(tr("Disconnect"));
        this->setButtonColor(this->start, QColor(RED));
        this->setButtonColor(this->record_data, QColor(GREEN));
    }
    else
    {
        this->terminal->typeNoPort();
        this->connected = false;
    }
}

void RealTimeDisp::disconnectSerialPort()
{
    if(this->serial->endConnection())
    {
        this->terminal->typeEndCommunication();
        this->connected = false;
        this->serial_timer->stop();
        // Change button visuals for connecting
        this->start->setText(tr("Connect"));
        this->setButtonColor(this->start, QColor(GREEN));
    }
    else
    {
        this->terminal->typeDisconnectFailed();
        this->connected = true;
    }
}

void RealTimeDisp::saveRecord(QString filename)
{
    std::ofstream record;
    std::string file_destination = RECORDS_PATH + filename.toStdString();
    record.open(file_destination, std::ios::out | std::ios::app);

    if(!record.is_open())
    {
        this->terminal->typeFileNotOpened();
        return;
    }

    // Write length of a signal, time interval and start a new line
    record << this->record_index + 1 << " " << PLOT_TIME << "\n";
    // Write whole signal
    for(uint16_t i = 0; i <= this->record_index; ++i)
    {
        record << this->recorded_data[i] << " ";
        if(record.fail())
        {
            this->terminal->typeErrorWhileSaving();
            return;
        }
    }
    record.close();
    // Reset frame
    this->record_index = 0;
    // Inform about successfull operation
    this->terminal->typeRecordSaved();
}























