#ifndef INFOEDIT_HPP
#define INFOEDIT_HPP

#include <QLineEdit>
#include <QLabel>
#include <QHBoxLayout>

class InfoEdit : public QWidget
{
    private:
        QLabel *value_info;
        QHBoxLayout *info_layout;

        void setInnerWidgets();
        void setInnerLayout();

    public:
        InfoEdit(const char *info, QWidget *parent = nullptr);

        QLineEdit *value_edit;

        QString returnText() { return this->value_edit->text(); }
        void placeText(QString new_text) { this->value_edit->setText(new_text); }
};

#endif // INFOEDIT_HPP
