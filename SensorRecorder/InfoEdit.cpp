#include "InfoEdit.hpp"

InfoEdit::InfoEdit(const char *info, QWidget *parent) : QWidget(parent)
{
    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);

    this->value_edit = new QLineEdit(this);
    this->value_info = new QLabel(tr(info), this);
    this->info_layout = new QHBoxLayout(this);

    setInnerWidgets();
    setInnerLayout();
}

void InfoEdit::setInnerWidgets()
{
    this->value_edit->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    this->value_info->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);

    this->value_edit->setAlignment(Qt::AlignLeft);
}

void InfoEdit::setInnerLayout()
{
    this->info_layout->addWidget(this->value_info);
    this->info_layout->addItem(new QSpacerItem(30, 1, QSizePolicy::Preferred, QSizePolicy::Preferred));
    this->info_layout->addWidget(this->value_edit);
}
