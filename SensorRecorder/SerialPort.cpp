#include "SerialPort.hpp"

/*!
 * \file
 * \brief Definicje metod klasy SerialPort
 */

/*!
 * \brief Inicjalizuje obiekt klasy SerialPort
 *
 * Konstruktor domyślny.
 */
SerialPort::SerialPort()
{
    // Zero all array elements
    for(int i = 0; i < FRAME_LENGTH; ++i)
        this->emg_data[i] = 0;
}
/*!
 * \brief Otwiera port szeregowy
 *
 * Otwiera port szeregowy w trybie ReadOnly. Jeżeli
 * operacja zostanie przeprowadzona pomyślnie, ustawiane
 * są odpowiednie parametry komunikacji:
 * \n- BaudRate = 115200
 * \n- StopBits = 1
 * \n- DataBIts = 8
 * \n- Parity = None
 *
 * \return Zwraca true, jeżeli port został otworzony i skonfigurowany
 *         lub false w przeciwnym wypadku.
 */
bool SerialPort::startConnection()
{
    bool serial_opened = false;

    port_desc = open(PORT_NAME, O_RDWR | O_NONBLOCK);

    if(port_desc > 0)
    {
        SetTransParam(port_desc);

        keep_reading = true;
        serial_opened = true;
    }

    return serial_opened;
}
/*!
 * \brief Kończy połączenie z portem szeregowym
 *
 * Zamyka otwarty deskryptor i powoduje zakończenie pracy
 * wątku, dzięki ustawieniu flagi keep_reading na fałsz.
 */
bool SerialPort::endConnection()
{
    close(port_desc);
    keep_reading = false;

    return true;
}
/*!
 * \brief Czyta dane z portu
 *
 * Tworzy tablicę na ramkę danych a następnie wczytuje
 * ramkę z portu. Ramkę przekazuje do funkcji parseDataFrame()
 * , która zwraca true jeżeli ramka jest prawidłowa lub false
 * jeżeli jest niekompletna lub błędnie odczytana.
 *
 * \param[in] data - obiekt przechowywujący wszystkie dane robota
 *
 * \return Zwraca true jeżeli czytanie i aktualizacja danych została
 *         przeprowadzona pomyślnie lub false w przeciwnym wypadku.
 */
bool SerialPort::receiveData()
{
    if(port_desc < 0)
        return false;

    std::string data_frame;
    bool read_status = false;

    if(RS232_Odbierz(this->returnDecriptor(), data_frame, FRAME_LIMIT, 100))
    {
        // Make sure that data frame end with '\0' symbol
        data_frame[data_frame.length()] = '\0';
        char *new_frame = new char[data_frame.length() + 1];
        std::copy(data_frame.begin(), data_frame.end(), new_frame);
        // Parse fresh data frame and return result
        read_status = parseDataFrame(new_frame, this->emg_data, data_frame.length());
    }

    return read_status;
}
/*!
 * \brief Analizuje otrzymaną ramkę danych
 *
 * Ramkę w postaci tablicy rzutuje na strumień danych.
 * Sprawdzana jest poprawność ramki (pierwszy znak) a
 * następnie dane zostają wczytane i aktualizowane w
 * obiekcie RobotData.
 *
 * \param[in] data_frame - ramka danych
 * \param[in] data - obiekt przechowywujący wszystkie dane robota
 *
 * \return Zwraca true jeżeli dane zostały wczytane poprawnie lub false
 *         jeżeli znak startu ramki sie nie zgadzał lub nastąpił błąd
 *         strumienia wejściowego.
 */
bool parseDataFrame(const char *data_frame, uint16_t *data_buffer, int data_length)
{
    // Rzutowanie na strumień wejściowy
    std::istringstream readed_line(data_frame);
    char header;
    // Wczytanie znaku startu
    readed_line >> header;

    // Sprawdzenie czy początek linii się zgadza
    if(header != 'X' || readed_line.fail() || data_length <= 1)
        return false;

    // Write all numbers into an array
    uint16_t value = 0;
    int counter = 0;
    while(true)
    {
        readed_line >> value;

        if(readed_line.fail())
            return false;

        data_buffer[counter] = value;
        ++counter;

        if(counter >= FRAME_LENGTH)
            break;
    }

    return true;
}
/*!
 * \brief Sprawdza zgodność sumy kontrolnej CRC
 *
 * Oczyszcza ramkę danych wydobywając tylko te użyteczne
 * wartości a następnie liczy dla takiej tablicy sumę
 * kontrolną, która to jest porównywana z otrzymaną w ramce.
 */
bool checkCRC(char *data_frame, int data_length, char crc)
{
    char clear_data[data_length - 5];
    std::copy(&data_frame[2], &data_frame[data_length - 3], clear_data);
    clear_data[data_length - 3] = '\0';
    // Tablica clear_data teraz zawiera wszystkie użyteczne dane ze znakiem terminatora
    char new_crc = crc8(clear_data, std::strlen(clear_data) - 1);

    if(new_crc != crc)
        return false;

    return true;
}
/*!
 * \brief Oblicza sumę kontrolną otrzymanej ramki danych
 */
char crc8(char *data_frame, int data_length)
{
  char *data = data_frame;
  char crc = 0x00;
  char extract;
  char sum;
  for(int i = 0; i <= data_length; ++i)
  {
    extract = *data;
    for (char tempI = 8; tempI; tempI--)
    {
      sum = (crc ^ extract) & 0x01;
      crc >>= 1;
      if(sum)
      crc ^= 0x8C;
      extract >>= 1;
    }
    data++;
  }
  return crc;
}
