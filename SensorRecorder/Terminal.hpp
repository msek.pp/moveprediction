#ifndef TERMINAL_HPP
#define TERMINAL_HPP

#include <QTextEdit>
#include <QDateTime>
#include <FramedWidget.hpp>

#define PLAIN "#0A0A0A"
#define ERROR_MSG "#FF0000"
#define SUCCESS_MSG "#00FF00"

enum TEXT_FORMAT
{
    PLAIN_FR,
    ERROR_FR,
    SUCCESS_FR
};

class Terminal : public FramedWidget
{
  Q_OBJECT

  private:
    QTextEdit *editor;
    QTextCharFormat underline;
    QTextCharFormat plain;
    QTextCharFormat error_fr;
    QTextCharFormat success_fr;

    const QString getTimeString();
    void typeFormatMsg(const QString & message, QTextCharFormat format);

    virtual void setVerticalLayout() override;
    virtual void setStoredWidgets() override;

  public:
    Terminal(const char *widget_title, QWidget *parent = nullptr);
    void setBackgroundColor(const QPalette & palette);

    friend Terminal & operator<< (Terminal & term, const char *message);

    void typeDeviceNotFound();
    void typeNoSerialPort();
    void typeStartCommunication();
    void typeEndCommunication();
    void typeDisconnectFailed();
    void typeNoPort();
    void typeOverflowError();
    void typeFileNotOpened();
    void typeErrorWhileSaving();
    void typeRecordSaved();
    void typeImportError();
    void typeFileImportError();
    void typeRecordImported(int data_length);
    void typeWrongClassName();
    void typeSampleSaved();
    void typeSampleSaveError();
};

/*!
 * \brief Overloaded << operator for displaing messages
 */
Terminal & operator<< (Terminal & term, const char *message);

#endif // TERMINAL_HPP
