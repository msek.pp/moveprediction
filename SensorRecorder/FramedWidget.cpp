#include "FramedWidget.hpp"

/*!
 * \file
 * \brief Definicja kostruktora klasy FramedWidget
 */

/*!
 * \brief Inicjalizuje obiekt klasy FramedWidget
 *
 *  Wymusza wywołanie konstruktora klasy QFrame, przekazując
 *  wskaźnik na rodzica. Tworzy tytuł widgetu oraz zarządcę
 *  wertykalnego. Na koniec edytuje wygląd samej ramki, która
 *  jest częścią wspólną każdego widgetu w oknie aplikacji.
 *
 * \param[in] widget_title - tytuł widgetu
 * \param[in] parent - wskaźnik na widget będący rodzicem
 */
FramedWidget::FramedWidget(const char *widget_title, QWidget *parent) : QFrame(parent)
{
    // Tworzy tytuł widgetu i zarządcę wertykalnego
    title = new QLabel(tr(widget_title));
    v_layout = new QVBoxLayout(this);
    // Nadaje odpowiedni styl obramowaniu
    setFrameStyle(QFrame::StyledPanel | QFrame::Raised);
    // Grubość linii obramowania (maksymalna)
    setLineWidth(3);
}
