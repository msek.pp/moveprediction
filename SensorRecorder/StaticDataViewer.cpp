#include "StaticDataViewer.hpp"

StaticDataViewer::StaticDataViewer(const char *plot_title, QWidget *parent) : DataViewer(plot_title, parent)
{
    QPen pen(QColor(140, 0, 0));
    pen.setWidth(1);
    for(int i = 0; i < 2; ++i)
    {
        this->signal_win[i] = new QCPItemLine(this->plot);
        this->signal_win[i]->setPen(pen);
        this->signal_win[i]->start->setCoords(0, this->min_y_value);
        this->signal_win[i]->end->setCoords(0, this->max_y_value);
    }

    setStoredWidgets();
}

void StaticDataViewer::setStoredWidgets()
{
    plot->addGraph();

    plot->yAxis->setRange(0, 6);
    plot->xAxis->setRange(0, 10, Qt::AlignLeft);
    plot->yAxis->setLabel("sEMG");
    plot->xAxis->setLabel("t[ms]");

    plot->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);

    plot->xAxis2->setVisible(true);
    plot->yAxis2->setVisible(true);
    plot->xAxis2->setTickLabels(false);
    plot->yAxis2->setTickLabels(false);
    plot->xAxis2->setTicks(false);
    plot->yAxis2->setTicks(false);
    plot->xAxis2->setSubTicks(false);
    plot->yAxis2->setSubTicks(false);
    plot->xAxis->setBasePen(QPen(Qt::white, 0.8));
    plot->yAxis->setBasePen(QPen(Qt::white, 0.8));
    plot->xAxis2->setBasePen(QPen(Qt::white, 0.8));
    plot->yAxis2->setBasePen(QPen(Qt::white, 0.8));
    plot->xAxis->setTickPen(QPen(Qt::white, 0.8));
    plot->yAxis->setTickPen(QPen(Qt::white, 0.8));
    plot->xAxis->setSubTickPen(QPen(Qt::white, 0.8));
    plot->yAxis->setSubTickPen(QPen(Qt::white, 0.8));
    plot->xAxis->setTickLabelColor(Qt::white);
    plot->yAxis->setTickLabelColor(Qt::white);
    plot->xAxis->setLabelColor(Qt::white);
    plot->yAxis->setLabelColor(Qt::white);

    plot->addGraph();

    plot->graph(0)->setPen(QPen(QColor(200, 220, 255), 0.8));
    plot->graph(1)->setPen(QPen(QColor(255, 69, 0), 0.8));

    connect(plot->xAxis, SIGNAL(rangeChanged(QCPRange)), plot->xAxis2, SLOT(setRange(QCPRange)));
    connect(plot->yAxis, SIGNAL(rangeChanged(QCPRange)), plot->yAxis2, SLOT(setRange(QCPRange)));

    setPlotGradient(QGradient(QGradient::EternalConstance));
}

void StaticDataViewer::addNewValue(int new_value, int time_interval)
{
    plot->graph(0)->addData(time_interval, new_value);
}

void StaticDataViewer::addNewValue(float new_value, int time_interval, int plot_num)
{
    plot->graph(plot_num)->addData(time_interval, new_value);
}

void StaticDataViewer::resetGraph()
{
    plot->graph(0)->data().data()->clear();
    plot->graph(1)->data().data()->clear();
    plot->xAxis->setRange(0, 10, Qt::AlignLeft);
    plot->replot();
}

void StaticDataViewer::placeWindow(int start, int win_size)
{
    // Place window on plot
    this->min_y_value = this->plot->yAxis->range().lower;
    this->max_y_value = this->plot->yAxis->range().upper;

    this->moveWindow(start, win_size);
}

void StaticDataViewer::moveWindow(int start, int win_size)
{
    this->signal_win[0]->start->setCoords(start - 1, this->min_y_value);
    this->signal_win[0]->end->setCoords(start - 1, this->max_y_value);
    this->signal_win[1]->start->setCoords(start + win_size + 1, this->min_y_value);
    this->signal_win[1]->end->setCoords(start + win_size + 1, this->max_y_value);

    // Might be problems
    this->plot->replot();
}
