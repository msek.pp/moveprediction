#include "SampleMaker.hpp"

SampleMaker::SampleMaker(QWidget *parent) : SignalViewer(parent)
{
    for(int i = 0; i < CLASSES_NUM; ++i)
        this->file_counter[i] = 0;

    this->file_counter[0] = 40;
    this->file_counter[1] = 40;
    this->file_counter[5] = 40;

    this->setFocusPolicy(Qt::StrongFocus);
    this->setEnabled(true);

    this->plot = new StaticDataViewer("Signal record", this);
    this->load_record = new QPushButton("Import record", this);
    this->save_sample = new QPushButton("Save", this);
    this->window_size = new InfoEdit("Window length: ", this);
    this->data_label = new InfoEdit("Data label: ", this);
    this->import_popup = new PopUpWin("Import record", this);

    this->smoother = new Filters(MOVING_WIN);

    this->setInnerWidgets();
    this->setInnerLayouts();

    // Connect pressing 'Import' button with showing popup
    connect(this->load_record, SIGNAL(pressed()), this, SLOT(openImportPopup()));
    // Connect closing pop up window with importing record
    connect(this->import_popup, SIGNAL(recordFileName(QString)), this, SLOT(importRecord(QString)));
    // Connect finishing writing in window size edit with displaing window
    connect(this->window_size->value_edit, SIGNAL(editingFinished()), this, SLOT(drawWindow()));
    // Connect pressing 'Save' button with saving sample cutted from signal by window
    connect(this->save_sample, SIGNAL(pressed()), this, SLOT(saveCuttedSample()));
}

void SampleMaker::setInnerWidgets()
{
    this->load_record->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    this->save_sample->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    this->setButtonProp(this->load_record);
    this->setButtonProp(this->save_sample);
    this->setButtonColor(this->load_record, QColor(NEUTRAL));
    this->setButtonColor(this->save_sample, QColor(NEUTRAL));
}

void SampleMaker::setInnerLayouts()
{
    this->buttons_layout->addWidget(this->load_record);
    this->buttons_layout->addItem(new QSpacerItem(10, 10, QSizePolicy::Minimum, QSizePolicy::Preferred));
    this->buttons_layout->addWidget(this->save_sample);
    this->buttons_layout->addItem(new QSpacerItem(10, 10, QSizePolicy::Minimum, QSizePolicy::Preferred));
    this->buttons_layout->addWidget(this->window_size);
    this->buttons_layout->addWidget(this->data_label);

    this->hor_layout->addLayout(this->buttons_layout);
    this->hor_layout->addWidget(this->terminal);

    this->main_layout->addWidget(this->plot);
    this->main_layout->addItem(new QSpacerItem(30, 30, QSizePolicy::Minimum, QSizePolicy::Preferred));
    this->main_layout->addLayout(this->hor_layout);
}

void SampleMaker::openImportPopup()
{
    this->import_popup->show();
}

void SampleMaker::importRecord(QString filename)
{
    std::ifstream record;

    std::string import_path = RECORDS_PATH + filename.toStdString();

    record.open(import_path);

    if(!record.is_open())
    {
        this->terminal->typeFileImportError();
        return;
    }

    unsigned int time_interval = 0; // ms
    record >> this->data_length >> time_interval;
    // Check if data in file is not corrupted
    if(this->data_length <= 0 || time_interval <= 0)
    {
        this->terminal->typeImportError();
        return;
    }
    // Clear current plot
    this->plot->resetGraph();

    // Static array for imported data - clear previous data
    if(this->recorded_signal != nullptr)
        delete [] this->recorded_signal;

    if(this->filtered_signal != nullptr)
        delete [] this->filtered_signal;

    this->recorded_signal = new float[this->data_length];
    this->filtered_signal = new float[this->data_length];

    float sample = 0.0;
    uint16_t time_elapsed = 0;

    for(uint16_t i = 0; i < this->data_length; ++i)
    {
        record >> sample;
        // Check if stream didn't failed
        if(record.fail())
        {
            this->terminal->typeImportError();
            return;
        }

        this->recorded_signal[i] = sample;
    }

    for(uint16_t i = 0; i < this->data_length; ++i)
    {
        if((i < MOVING_WIN / 2) || (i >= this->data_length - (MOVING_WIN / 2) - 1))
            this->filtered_signal[i] = 0;
        else
            this->filtered_signal[i] = this->smoother->TriangleAverageFilter(this->recorded_signal, i - (MOVING_WIN / 2));

        this->plot->addNewValue(this->recorded_signal[i], time_elapsed);
        this->plot->addNewValue(this->filtered_signal[i], time_elapsed, 1);

        time_elapsed += time_interval;
    }

    this->plot->fitAxesToData();
    this->plot->replot();

    this->terminal->typeRecordImported(data_length);

    this->win_start_index = 0;

    record.close();
}

void SampleMaker::drawWindow()
{
    // If there is no signal imported, return
    if(this->recorded_signal == nullptr)
        return;

    // Get window size from user
    QString win_size_text = this->window_size->returnText();
    this->win_size = win_size_text.toInt();
    // Some secure from too small or too big window size
    if(this->win_size <= 1 || this->win_size >= this->data_length)
    {
        this->win_size = 10;
        this->window_size->placeText("10 (default)");
    }

    this->win_start_index = 0;

    this->plot->placeWindow(this->win_start_index, this->win_size * PLOT_TIME);
}

void SampleMaker::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Left)
    {
        this->win_start_index -= 5;
        this->win_start_index = (this->win_start_index <= 0) ? 0 : this->win_start_index;

        this->plot->moveWindow(this->win_start_index * PLOT_TIME, this->win_size * PLOT_TIME);
    }
    else if(event->key() == Qt::Key_Right)
    {
        this->win_start_index += 5;
        this->win_start_index = (this->win_start_index >= this->data_length - this->win_size) ?
                                (this->data_length - this->win_size - 1) : this->win_start_index;

        this->plot->moveWindow(this->win_start_index * PLOT_TIME, this->win_size * PLOT_TIME);
    }
}

void SampleMaker::saveCuttedSample()
{
    // If no record is imported, return
    if(this->recorded_signal == nullptr)
        return;
    // Get class from user and check if its correct
    std::string data_class = this->data_label->value_edit->text().toStdString();
    bool class_correct = false;
    // Variable to remember class index from a corresponding array
    int class_index = 0;
    for(int i = 0; i < CLASSES_NUM; ++i)
    {
        if(data_class == signal_classes[i])
        {
            class_correct = true;
            class_index = i;
            break;
        }
    }
    // Return if there is mistake in a class name
    if(!class_correct)
    {
        this->terminal->typeWrongClassName();
        return;
    }
    // Save to file
    std::ofstream sample;
    // Create file name
    data_class[0] = toupper(data_class[0]);
    std::string filename = data_class + "Sig" + std::to_string(this->win_size) +
                           "_" + std::to_string(this->file_counter[class_index]);

    std::string file_destination = SAMPLES_PATH + signal_classes[class_index] + "/" + filename;

    sample.open(file_destination, std::ios::out | std::ios::app);

    if(!sample.is_open())
    {
        this->terminal->typeFileNotOpened();
        return;
    }
    // Update files counters! Edit to use JSON
    this->file_counter[class_index] += 1;
    // Check indices, protection from segmentation fault
    uint16_t start = (this->win_start_index < 0) ? 0 : this->win_start_index;
    uint16_t end = (this->win_start_index + this->win_size >= this->data_length) ?
                   (this->data_length - this->win_size - 1) : (this->win_start_index + this->win_size);

    if(start >= end)
    {
        terminal->typeSampleSaveError();
        return;
    }
    sample << this->win_size << " " << PLOT_TIME << "\n";
    // Write whole signal
    for(uint16_t i = start; i < end; ++i)
    {
        sample << this->filtered_signal[i] << " ";
        if(sample.fail())
        {
            this->terminal->typeSampleSaveError();
            return;
        }
    }
    sample.close();
    this->terminal->typeSampleSaved();
}

SampleMaker::~SampleMaker()
{
    delete [] this->recorded_signal;
    delete [] this->filtered_signal;
}




















