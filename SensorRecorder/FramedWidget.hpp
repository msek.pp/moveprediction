#ifndef FRAMEDWIDGET_HPP
#define FRAMEDWIDGET_HPP

/*!
 * \file
 * \brief Definicja klasy FramedWidget
 *
 * Plik zawiera definicję klasy FramedWidget, która
 * jest klasą bazową dla wszystkich widgetów wyświetlanych
 * w oknie głównym.
 */

#include <QFrame>
#include <QLabel>
#include <QVBoxLayout>

/*!
 * \brief Klasa bazowa głównych widgetów okna aplikacji
 *
 * Definiuje ona dwie metody wirtualne oraz przechowywuje atrubuty,
 * które są uniwersalne dla każdego widgetu pochodnego. Dziedziczy
 * po klasie QFrame, co nadaje widgetom pochodnym obramowanie (stąd
 * nazwa FramedWidget).
 */
class FramedWidget : public QFrame
{
  protected:
    /*!
     * \brief Ustawia przechowywane widgety w pionie
     *
     * Metoda czysto wirtualna. Każdy widget pochodny ma co
     * najmniej dwa widgety, które należy ustawić w pionie -
     * tytuł oraz widget główny, np. QTextEditor.
     */
    virtual void setVerticalLayout() = 0;
    /*!
     * \brief Nadaje odpowiednie parametry widgetowi
     *
     * Metoda czysto wirtualna. Każdy widget pochodny
     * ustawia odpwiedni wygląd, geometrię czy zachowanie widgetowi
     * przechowywanemu.
     */
    virtual void setStoredWidgets() = 0;

  public:
    /*!
     * \brief Tytuł widgetu
     */
    QLabel *title;
    /*!
     * \brief Zarządca geometrii wertykalnej
     */
    QVBoxLayout *v_layout;
    /*!
     * \brief Konstruktor domyślny
     */
    FramedWidget() { }
    /*!
     * \brief Inicjalizuje obiekt klasy FramedWidget
     */
    FramedWidget(const char *widget_title, QWidget *parent);
    /*!
     * \brief Zmienia nazwę widgetu / tłumaczy nazwę
     */
    void changeTitle(QString new_title) { title->setText(new_title); }
    /*!
     * \brief Wirtualny destruktor
     */
    virtual ~FramedWidget() { }
};

#endif // FRAMEDWIDGET_HPP
