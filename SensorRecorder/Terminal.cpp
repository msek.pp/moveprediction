#include "Terminal.hpp"

Terminal::Terminal(const char *terminal_title, QWidget *parent) : FramedWidget(terminal_title, parent)
{
    editor = new QTextEdit(this);
    // Definicja zachowania geometrii
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

    setStoredWidgets();
    setVerticalLayout();
}
/*!
 * \brief Inicjalizuje widgety pochodne
 *
 * Konfiguruje atrybut editor, nadając mu odpowiedni kolor, zachowanie
 * geometrii oraz tryb ReadOnly.
 */
void Terminal::setStoredWidgets()
{
    editor->setReadOnly(true);
    editor->setStyleSheet("QTextEdit { background-color: rgb(128, 128, 128); }");
    editor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    plain = editor->textCursor().charFormat();
    plain.setForeground(QBrush(QColor(PLAIN)));

    underline.setForeground(QBrush(QColor(PLAIN)));
    underline.setFontWeight(QFont::Bold);

    error_fr.setForeground(QBrush(QColor(ERROR_MSG)));

    success_fr.setForeground(QBrush(QColor(SUCCESS_MSG)));
}
/*!
 * \brief Ustawia widgety pochodne w pionie
 *
 * Idąc od góry, ustawiany jest tytuł a następnie edytor.
 */
void Terminal::setVerticalLayout()
{
    v_layout->addWidget(title);
    v_layout->addWidget(editor);
}
/*!
 * \brief Ustawia tło edytora
 *
 * \param palette - skonfigurowana paleta barw
 */
void Terminal::setBackgroundColor(const QPalette & palette)
{
    editor->setAutoFillBackground(true);
    editor->setPalette(palette);
}
/*!
 * \brief Zwraca aktualny czas w formie tekstu
 *
 * \return Zwraca stały obiekt typu QString zawierający datę w formacie
 *         hh:mm:ss.
 */
const QString Terminal::getTimeString()
{
    QDateTime date = QDateTime::currentDateTime();
    QString time = date.toString("hh:mm:ss");

    return ("~> " + time + ": ");
}

/*!
 * \brief Wypisuje komunikat z żądanym formatowaniem poprzedzając go
 *
 * \param message - komunikat do wyświeltenia
 * \param format - styl pisanego tekstu
 */
void Terminal::typeFormatMsg(const QString &message, QTextCharFormat format)
{
    editor->moveCursor(QTextCursor::End);
    editor->textCursor().insertText(message, format);
    editor->moveCursor(QTextCursor::End);
}

void Terminal::typeStartCommunication()
{
  this->typeFormatMsg(getTimeString(), this->plain);
  this->typeFormatMsg(tr("Successfully started communication with the device.\n\n"), this->success_fr);
}

void Terminal::typeEndCommunication()
{
  this->typeFormatMsg(getTimeString(), this->plain);
  this->typeFormatMsg(tr("Ended communication with the device.\n\n"), this->plain);
}

void Terminal::typeDisconnectFailed()
{
    this->typeFormatMsg(getTimeString(), this->plain);
    this->typeFormatMsg(tr("Failed disconnecting serial port.\n\n"), this->error_fr);
}

void Terminal::typeNoPort()
{
  this->typeFormatMsg(getTimeString(), this->plain);
  this->typeFormatMsg(tr("Cannot open serial port.\n\n"), this->error_fr);
}

void Terminal::typeDeviceNotFound()
{
    this->typeFormatMsg(getTimeString(), this->plain);
    this->typeFormatMsg(tr("Device not found.\n\n"), this->error_fr);
}

void Terminal::typeNoSerialPort()
{
    this->typeFormatMsg(getTimeString(), this->plain);
    this->typeFormatMsg(tr("No serial ports available.\n\n"), this->error_fr);
}

void Terminal::typeOverflowError()
{
    this->typeFormatMsg(getTimeString(), this->plain);
    this->typeFormatMsg(tr("Signal buffer ended, have to save now.\n\n"), this->error_fr);
}

void Terminal::typeFileNotOpened()
{
    this->typeFormatMsg(getTimeString(), this->plain);
    this->typeFormatMsg(tr("File failed to open. Record not saved.\n\n"), this->error_fr);
}

void Terminal::typeImportError()
{
    this->typeFormatMsg(getTimeString(), this->plain);
    this->typeFormatMsg(tr("Error during record import.\n\n"), this->error_fr);
}

void Terminal::typeFileImportError()
{
    this->typeFormatMsg(getTimeString(), this->plain);
    this->typeFormatMsg(tr("File failed to open. Record not imported.\n\n"), this->error_fr);
}

void Terminal::typeErrorWhileSaving()
{
    this->typeFormatMsg(getTimeString(), this->plain);
    this->typeFormatMsg(tr("Error during saving file. Record not saved.\n\n"), this->error_fr);
}

void Terminal::typeRecordSaved()
{
    this->typeFormatMsg(getTimeString(), this->plain);
    this->typeFormatMsg(tr("Record successfully saved.\n\n"), this->success_fr);
}

void Terminal::typeRecordImported(int data_length)
{
    QString message = QString("\t Signal length: %1 samples.\n\n").arg(data_length);
    this->typeFormatMsg(getTimeString(), this->plain);
    this->typeFormatMsg(tr("Record successfully imported.\n"), this->success_fr);
    this->typeFormatMsg(message, this->plain);
}

void Terminal::typeWrongClassName()
{
    this->typeFormatMsg(getTimeString(), this->plain);
    this->typeFormatMsg(tr("Wrong class name for signal.\n\n"), this->error_fr);
}

void Terminal::typeSampleSaved()
{
    this->typeFormatMsg(getTimeString(), this->plain);
    this->typeFormatMsg(tr("Sample successfully saved.\n\n"), this->success_fr);
}

void Terminal::typeSampleSaveError()
{
    this->typeFormatMsg(getTimeString(), this->plain);
    this->typeFormatMsg(tr("Error during saving a sample.\n\n"), this->error_fr);
}

Terminal & operator<< (Terminal & term, const char *message)
{
    term.typeFormatMsg(term.getTimeString(), term.plain);
    term.typeFormatMsg(message, term.plain);
}
