#include "Filters.hpp"

Filters::Filters(uint8_t window, double gauss_spread)
{
    this->window_size = window;
    this->sigma = gauss_spread;

    this->gauss_weights = new double[this->window_size];
    this->triangle_weights = new uint16_t[this->window_size];

    this->countGaussWeights();
    this->countTriangleWeights();
}

uint16_t Filters::MedianFilter(uint16_t *input_signal, uint16_t start)
{
    uint16_t window[this->window_size];

    for(int i = 0; i < this->window_size; ++i)
        window[i] = input_signal[start + i];

    std::sort(window, window + this->window_size);

    return window[this->window_size / 2];
}

uint16_t Filters::AverageFilter(uint16_t *input_signal, uint16_t start)
{
    uint16_t output = 0;

    if(this->last_filtered == 0)
    {
        for(int i = 0; i < this->window_size; ++i)
            output += input_signal[start + i];
    }
    else
        output = this->last_filtered + input_signal[start + this->window_size] - input_signal[start - 1];

    this->last_filtered = output;

    return (output / this->window_size);
}

uint16_t Filters::TriangleAverageFilter(uint16_t *input_signal, uint16_t start)
{
    uint16_t output = 0;

    for(int i = 0; i < this->window_size; ++i)
        output += input_signal[start + i] * this->triangle_weights[i];

    return (output / this->triangle_weights_sum);
}

float Filters::TriangleAverageFilter(float *input_signal, uint16_t start)
{
    float output = 0;

    for(int i = 0; i < this->window_size; ++i)
        output += input_signal[start + i] * (float)this->triangle_weights[i];

    return (output / this->triangle_weights_sum);
}

uint16_t Filters::GaussianAverageFilter(uint16_t *input_signal, uint16_t start)
{
    uint16_t output = 0;

    for(int i = 0; i < this->window_size; ++i)
        output += input_signal[start + i] * (uint16_t)this->gauss_weights[i];

    return output;
}

void Filters::countTriangleWeights()
{
    uint16_t weight_count = (this->window_size / 2) + 1;

    // Count weights for first half
    for(int i = 0; i < (this->window_size / 2) + 1; ++i)
        this->triangle_weights[(this->window_size / 2) - i] = weight_count - i;
    // Mirror first half to second half
    for(int i = (this->window_size / 2) + 1; i < this->window_size; ++i)
        this->triangle_weights[i] = this->triangle_weights[this->window_size - i - 1];

    this->triangle_weights_sum = 0;
    for(int i = 0; i < this->window_size; ++i)
        this->triangle_weights_sum += this->triangle_weights[i];

    for(int i = 0; i < this->window_size; ++i)
        qDebug() << this->triangle_weights[i] << "\n";
}

void Filters::countGaussWeights()
{
    double gauss_const = 1 / (sqrt(2 * M_PI * pow(sigma, 2)));

    // Count weights for first half
    for(int i = 0; i < (this->window_size / 2) + 1; ++i)
        this->gauss_weights[i] = -exp(-0.5 * pow(((double)(i + 1)/this->sigma), 2));
    // Mirror first half to second half
    for(int i = (this->window_size / 2) + 1; i < this->window_size; ++i)
        this->gauss_weights[i] = -this->gauss_weights[this->window_size - i - 1];
}

Filters::~Filters()
{
    delete [] this->gauss_weights;
    delete [] this->triangle_weights;
}
