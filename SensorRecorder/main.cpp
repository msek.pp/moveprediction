#include "Mainwindow.hpp"

#include <QApplication>

int main(int argc, char *argv[])
{   
    QApplication app(argc, argv);
    MainWindow window;

    window.show();

    int result = app.exec();

    return result;
}
