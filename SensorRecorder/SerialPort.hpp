#ifndef SERIALPORT_HPP
#define SERIALPORT_HPP

#include <condition_variable>
#include <thread>
#include <sstream>
#include <cstring>
#include <iostream>
#include <vector>
#include <fcntl.h>
#include <cstdio>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <QDebug>
#include "Receiving.hpp"
#include "Transparam.hpp"

#define FRAME_LENGTH 10
#define FRAME_LIMIT 70
#define PORT_NAME "/dev/ttyACM0"

/*!
 * \brief Modeluje port szeregowy
 *
 * Klasa SerialPort dodaje nowe sloty do klasy
 * QSerialPort, które są implementowane z myślą
 * o komunikacji z robotem.
 */
class SerialPort
{
  private:
    /*!
     * \brief Buffer with fixed length for sEMG sensor's data
     */
    uint16_t emg_data[FRAME_LENGTH];
    /*!
     * \brief Zmienna decydująca o działaniu wątku
     */
    bool keep_reading = false;
    /*!
     * \brief Boolean with information whether there is new data in buffer
     */
    bool new_data = false;
    /*!
     * \brief Deskryptor pliku portu szeregowego
     */
    int port_desc = -1;
    /*!
     * \brief Zwraca deskryptor pliku
     *
     * \return Liczba symbolizująca deskryptor
     */
    int returnDecriptor() const { return port_desc; }

  public:
    /*!
     * \brief Inicjalizuje obiekt klasy SerialPort
     */
    SerialPort();
    /*!
     * \brief Metoda decydująca o kontynuacji czytania
     *
     * \return True dla dalszego czytania lub False dla przerwania
     */
    bool keepReading() { return keep_reading; }
    /*!
     * \brief Odbiera dane z portu
     *
     * \return True jeżeli wczytanie się powiodło lub False jeżeli nie
     */
    bool receiveData();
    /*!
     * \brief Otwiera port szeregowy
     */
    bool startConnection();
    /*!
     * \brief Kończy połączenie z portem szeregowym
     */
    bool endConnection();
    /*!
     * \brief Reset value of corresponding variable if data were readed
     */
    void dataReceived() { this->new_data = false; }
    /*!
     * \brief Returns array with sEMG data
     *
     * \return emg_data - Pointer to the beginning of an INT array
     */
    uint16_t *retEMGData() { return this->emg_data; }
    bool newDataReceived() const { return this->new_data; }
};

/*!
 * \brief Analizuje otrzymaną ramkę danych
 */
bool parseDataFrame(const char *data_frame, uint16_t *data_buffer, int data_length);
/*!
 * \brief Sprawdza zgodność sumy kontrolnej CRC
 */
bool checkCRC(char *data_frame, int data_length, char crc);
/*!
 * \brief Oblicza sumę kontrolną otrzymanej ramki danych
 */
char crc8(char *data_frame, int data_length);

#endif // SERIALPORT_HPP
