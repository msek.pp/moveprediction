#include "Mainwindow.hpp"
#include "ui_Mainwindow.h"

MainWindow::MainWindow(QWidget *parent, unsigned int x, unsigned int y) : QMainWindow(parent)
{
    this->setFocusPolicy(Qt::StrongFocus);

    this->main_widget = new QWidget(this);
    this->setCentralWidget(this->main_widget);

    this->setStatusBar(new QStatusBar());
    this->statusBar()->showMessage(tr("Application is running..."));

    this->resize(x, y);

    this->windows = new QTabWidget(this->main_widget);
    this->rt_data_viewer = new RealTimeDisp();
    this->sample_maker = new SampleMaker();

    this->windows->addTab(this->rt_data_viewer, tr("Real time data viewer"));
    this->windows->addTab(this->sample_maker, tr("Sample maker"));

    this->main_layout = new QHBoxLayout(this->main_widget);
    main_layout->addWidget(this->windows);
}

MainWindow::~MainWindow()
{

}

