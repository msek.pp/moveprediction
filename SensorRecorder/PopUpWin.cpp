#include "PopUpWin.hpp"

PopUpWin::PopUpWin(const char *popup_title, QWidget *parent) : QDialog(parent)
{
    this->setWindowTitle(popup_title);
    this->setFixedWidth(POPUP_WIDTH);
    this->setFixedHeight(POPUP_HEIGHT);

    this->confirm = new QPushButton(tr("Confirm"), this);
    this->cancel = new QPushButton(tr("Cancel"), this);
    this->file_insert = new QLineEdit(this);
    this->edit_name = new QLabel(tr("File name:"));
    this->vert_layout = new QVBoxLayout(this);
    this->line_edit_layout = new QHBoxLayout();
    this->buttons_layout = new QHBoxLayout();

    this->setInnerWidgets();
    this->setInnerLayouts();

    connect(this->confirm, SIGNAL(pressed()), this, SLOT(takeFilename()));
    connect(this->cancel, SIGNAL(pressed()), this, SLOT(quit()));
}

void PopUpWin::setInnerWidgets()
{
    // Line edit modifications
    this->file_insert->setAlignment(Qt::AlignLeft);
    this->file_insert->setFrame(true);
    this->file_insert->setMaxLength(MAX_FILE_NAME);
    this->file_insert->setReadOnly(false);
    this->file_insert->setTextMargins(2, 1, 2, 1);
    this->file_insert->setPlaceholderText(tr("Type file name..."));

    this->file_insert->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    this->confirm->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    this->cancel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    this->edit_name->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
}

void PopUpWin::setInnerLayouts()
{
    this->line_edit_layout->addWidget(this->edit_name);
    this->line_edit_layout->addWidget(this->file_insert);
    this->buttons_layout->addWidget(this->confirm);
    this->buttons_layout->addWidget(this->cancel);

    this->vert_layout->addLayout(this->line_edit_layout);
    this->vert_layout->addItem(new QSpacerItem(10, 10, QSizePolicy::Minimum, QSizePolicy::Preferred));
    this->vert_layout->addLayout(this->buttons_layout);
}

void PopUpWin::takeFilename()
{
    // Emit signal passing new record's file name
    QString typed_text = this->file_insert->text();
    emit this->recordFileName(typed_text);
    this->done(0);
}

void PopUpWin::quit()
{
    // Quit QDialog window
    this->done(0);
}
