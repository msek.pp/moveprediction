#ifndef SDVIEWER_HPP
#define SDVIEWER_HPP

#include "DataViewer.hpp"

#define PLOT_MIN_HEIGHT 200

class StaticDataViewer : public DataViewer
{
    Q_OBJECT
    private:
        QCPItemLine *signal_win[2];
        double max_y_value = 0;
        double min_y_value = 0;

        void setStoredWidgets() override;

    public:
        StaticDataViewer(const char *plot_title, QWidget *parent = nullptr);

        void addNewValue(int new_value, int time_interval) override;
        void addNewValue(float new_value, int time_interval, int plot_num = 0) override;
        void resetGraph() override;
        void placeWindow(int start, int win_size) override;
        void moveWindow(int start, int win_size) override;
};

#endif // SDVIEWER_HPP
