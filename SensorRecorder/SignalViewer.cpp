#include "SignalViewer.hpp"

SignalViewer::SignalViewer(QWidget *parent) : QWidget(parent)
{
    //this->plot = new DataViewer("EMG signal", this);
    this->terminal = new Terminal("Terminal", this);
    this->main_layout = new QVBoxLayout(this);
    this->hor_layout = new QHBoxLayout();
    this->buttons_layout = new QVBoxLayout();

    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}

void SignalViewer::setButtonProp(QPushButton *button)
{
    button->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    button->setAutoFillBackground(true);
}

void SignalViewer::setButtonColor(QPushButton *button, QColor color)
{
    QPalette new_palette = button->palette();
    new_palette.setColor(QPalette::Button, color);
    button->setPalette(new_palette);

    button->update();
}
