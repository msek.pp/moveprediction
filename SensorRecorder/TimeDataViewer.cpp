﻿#include "TimeDataViewer.hpp"

TimeDataViewer::TimeDataViewer(const char *plot_title, QWidget *parent) : DataViewer(plot_title, parent)
{
    setStoredWidgets();
}

void TimeDataViewer::setStoredWidgets()
{
    plot->addGraph();

    plot->yAxis->setRange(0, 6);
    plot->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    plot->yAxis->setLabel("sEMG");
    plot->xAxis->setLabel("t[s]");

    plot->xAxis2->setVisible(true);
    plot->yAxis2->setVisible(true);
    plot->xAxis2->setTickLabels(false);
    plot->yAxis2->setTickLabels(false);
    plot->xAxis2->setTicks(false);
    plot->yAxis2->setTicks(false);
    plot->xAxis2->setSubTicks(false);
    plot->yAxis2->setSubTicks(false);
    plot->xAxis->setBasePen(QPen(Qt::white, 0.8));
    plot->yAxis->setBasePen(QPen(Qt::white, 0.8));
    plot->xAxis2->setBasePen(QPen(Qt::white, 0.8));
    plot->yAxis2->setBasePen(QPen(Qt::white, 0.8));
    plot->xAxis->setTickPen(QPen(Qt::white, 0.8));
    plot->yAxis->setTickPen(QPen(Qt::white, 0.8));
    plot->xAxis->setSubTickPen(QPen(Qt::white, 0.8));
    plot->yAxis->setSubTickPen(QPen(Qt::white, 0.8));
    plot->xAxis->setTickLabelColor(Qt::white);
    plot->yAxis->setTickLabelColor(Qt::white);
    plot->xAxis->setLabelColor(Qt::white);
    plot->yAxis->setLabelColor(Qt::white);

    plot->graph(0)->setPen(QPen(QColor(200, 220, 255), 0.8));

    plot->addGraph();
    plot->graph(1)->setPen(QPen(QColor(255, 69, 0), 0.8));

    QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
    timeTicker->setTimeFormat("%s:%z");
    plot->xAxis->setTicker(timeTicker);

    connect(plot->xAxis, SIGNAL(rangeChanged(QCPRange)), plot->xAxis2, SLOT(setRange(QCPRange)));
    connect(plot->yAxis, SIGNAL(rangeChanged(QCPRange)), plot->yAxis2, SLOT(setRange(QCPRange)));

    setPlotGradient(QGradient(QGradient::EternalConstance));
}

void TimeDataViewer::updateTimeElapsed(int time_interval_ms)
{
    // Conversion from miliseconds to seconds
    double key = time_interval_ms / 1000.0;
    // Update time elapsed from the start
    this->time_elapsed += key;
    // Update need for replot
    this->replot_time += key;
}

void TimeDataViewer::checkReplot()
{
    if(this->replot_time >= 0.05)
    {
        plot->replot();
        this->replot_time = 0;
    }
}

void TimeDataViewer::addNewValue(int new_value, int time_interval)
{
    this->updateTimeElapsed(time_interval);

    plot->graph(0)->addData(this->time_elapsed, new_value);
    plot->xAxis->setRange(this->time_elapsed, 1, Qt::AlignRight);

    this->checkReplot();
}

void TimeDataViewer::addNewValue(float new_value, int time_interval, int plot_num)
{
    // Just add filtered data moved back in time
    if(plot_num == 1)
        plot->graph(plot_num)->addData(time_elapsed - (double)(time_interval / 1000.0), new_value);
    else
    {
        this->updateTimeElapsed(time_interval);

        plot->graph(plot_num)->addData(time_elapsed, new_value);
        plot->xAxis->setRange(time_elapsed, 5, Qt::AlignRight);
    }

    this->checkReplot();
}

void TimeDataViewer::resetGraph()
{
    plot->graph(0)->data().data()->clear();

    this->time_elapsed = 0;
    this->replot_time = 0;

    plot->xAxis->setRange(time_elapsed + 5, 10, Qt::AlignRight);

    plot->replot();
}

void TimeDataViewer::placeWindow(int start, int win_size)
{

}

void TimeDataViewer::moveWindow(int start, int win_size)
{

}
