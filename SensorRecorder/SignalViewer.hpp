#ifndef SIGNALVIEWER_HPP
#define SIGNALVIEWER_HPP

#include "TimeDataViewer.hpp"
#include "Terminal.hpp"
#include "Filters.hpp"

#define RECORDS_PATH "/home/higgs_001/AI_EMG_Prediction/PredictionAI_sEMG/RecordedSignals/Recordings/"
#define SAMPLES_PATH "/home/higgs_001/AI_EMG_Prediction/PredictionAI_sEMG/RecordedSignals/DataSet/"

#define GREY "#878787"
#define GREEN "#059629"
#define RED "#FA2D02"
#define NEUTRAL "#09203F"

// Size of smoothing window
#define MOVING_WIN 11

#define PLOT_TIME 2

class SignalViewer : public QWidget
{
    protected:
        virtual void setInnerLayouts() = 0;
        virtual void setInnerWidgets() = 0;
        virtual void setButtonColor(QPushButton *button, QColor color);
        virtual void setButtonProp(QPushButton *button);

        DataViewer *plot;
        Terminal *terminal;
        QVBoxLayout *main_layout;
        QHBoxLayout *hor_layout;
        QVBoxLayout *buttons_layout;

    public:
        SignalViewer(QWidget *parent = nullptr);
        virtual ~SignalViewer() { }
};

#endif // SIGNALVIEWER_HPP
