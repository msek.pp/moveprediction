import numpy as np
import matplotlib.pyplot as plt


def normalization(signal, low_bound=0, high_bound=1):
    """Rescale signal to given range"""

    min_value = np.min(signal)
    max_value = np.max(signal)

    return low_bound + (signal - min_value)*(high_bound - low_bound)/(max_value - min_value)


def movingAverageFilter(signal, window=21):

    output = 0
    last_filtered = 0
    filtered = []

    for i in range(0, window):
        output += signal[i]
        last_filtered = output
    filtered.append(output/window)

    for x in range(1, (np.size(signal) - window + 1)):
        for i in range(0, window):
            output = last_filtered + signal[x + window - 1] - signal[x - 1]
        last_filtered = output
        filtered.append(output/window)

    filtered = np.array(filtered)

    return filtered


def standarization(signal):
    """Standardize signal"""

    return signal
