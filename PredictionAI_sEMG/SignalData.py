import math
import tensorflow as tf
import os
from SignalOperations import *


class SignalData:
    """Class representing EMG signals dataset"""

    class_labels = ["fist", "open_hand", "thumb_up", "two", "three", "four"]
    dataset = []
    test_split = 0  # percent
    train_split = 0  # percent
    valid_split = 0  # percent

    def __init__(self, test_div=15, train_div=70, valid_div=15):
        """Init object"""
        self.test_split = test_div
        self.train_split = train_div
        self.valid_split = valid_div

    def importTimeSeries(self, filename):
        """Function importing signal from given file. Returns numpy array."""

        f_desc = open(filename, 'r')

        data = f_desc.read()
        lines = data.split('\n')

        sample_list = []
        text_signal = lines[1].split()
        for sample in text_signal:
            sample_list.append(float(sample))

        f_desc.close()

        sample_list = np.array(sample_list)
        sample_list.astype(np.float64, copy=False)

        # For some tests
        filtered_signal = movingAverageFilter(sample_list)
        # filtered_signal = normalization(sample_list)

        return np.array(filtered_signal)

    def load(self, data_location):
        """Load dataset from given parent directory"""

        for label in self.class_labels:
            signal_files = os.listdir(data_location + "/" + label)

            for filename in signal_files:
                imported_signal = self.importTimeSeries(data_location + "/" + label + "/" + filename)
                record = {label: imported_signal}
                # record[label] = imported_signal
                self.dataset.append(record)

    def initTFDataSet(self):
        """Returns """

        # Count all needed sizes of subsets
        dataset_size = self.datasetSize()

        test_size = math.floor((self.test_split * dataset_size) / 100)
        valid_size = math.floor((self.valid_split * dataset_size) / 100)
        train_size = dataset_size - test_size - valid_size

        # Get separate lists of arrays for each label
        classes_list = self.splitDataAccordClasses()

        train_list = classes_list[:train_size]
        test_list = classes_list[train_size:(test_size + train_size)]
        valid_list = classes_list[(test_size + train_size):(valid_size + train_size + valid_size)]

        (train_features, train_labels) = self.createTensorSubset(train_list)
        (test_features, test_labels) = self.createTensorSubset(test_list)
        (valid_features, valid_labels) = self.createTensorSubset(valid_list)

        return train_features, train_labels, test_features, test_labels, valid_features, valid_labels

    def createTensorSubset(self, records, shuffle=True):
        """"""

        list_of_records = []
        for record in records:
            attr = list(record.keys())
            int_label = self.categoricalToInt(attr[0])
            signal = record[attr[0]]
            signal = np.append(signal, [int_label])
            list_of_records.append(signal)

        list_of_records = tf.convert_to_tensor(list_of_records)

        if shuffle:
            list_of_records = tf.random.shuffle(list_of_records)

        # Back to numpy, return signals and labels separately
        list_of_records = list_of_records.numpy()
        labels = list_of_records[:, -1]
        labels.astype(np.int8, copy=False)
        list_of_records = np.delete(list_of_records, -1, axis=1)

        return list_of_records, labels

    def categoricalToInt(self, string_label):
        """Converts string label into integer according to list of labels attribute"""

        return self.class_labels.index(string_label)

    def intToCategorical(self, int_label):
        """Converts integer label into string according to list of labels attribute"""

        return self.class_labels[int(int_label)]

    def datasetSize(self):
        """Returns number of record in data set"""

        return len(self.dataset)

    def splitDataAccordClasses(self):
        """Helping function splitting dataset according to class labels"""

        classes_list = []

        # Create 2D array of dictionaries (records)
        for label in self.class_labels:
            classes_list.append([])
            for record in self.dataset:
                attr = list(record.keys())
                if attr[0] == label:
                    classes_list[-1].append(record)
        # Create one list with records sorted by labels:
        # signals_1D = [fist, open_hand, thumbs_up, fist, open_hand, ... ]
        signals_1D = []
        for x in range(0, len(classes_list[0])):
            for y in range(0, len(classes_list)):
                signals_1D.append(classes_list[y][x])

        return signals_1D
