import tensorflow as tf
from SignalData import SignalData
from NNFunctions import predictionResults, plotHistory

# Some learning parameters ======================
hidden_layer_neurons = 256
epochs_num = 30
optimizer_fun = "adam"
loss_fun = "sparse_categorical_crossentropy"
# ===============================================

# Path to dataset
data_location = "RecordedSignals/DataSet"
# Load dataset as numpy arrays (train, test and valid)
data_reader = SignalData()
data_reader.load(data_location)
train_features, train_labels, test_features, test_labels, valid_features, valid_labels = data_reader.initTFDataSet()

model = tf.keras.models.Sequential([
        tf.keras.layers.Flatten(input_shape=(train_features.shape[1],)),
        tf.keras.layers.Dense(256, activation=tf.nn.relu),
        tf.keras.layers.Dense(128, activation=tf.nn.relu),
        tf.keras.layers.Dense(6, activation=tf.nn.softmax)
    ])
"""
model = tf.keras.models.Sequential([
        tf.keras.layers.Conv1D(filters=64, kernel_size=3, activation='relu', input_shape=[None, train_features.shape[1],1]),
        tf.keras.layers.Conv1D(filters=64, kernel_size=3, activation='relu'),
        tf.keras.layers.MaxPool1D(pool_size=3),
        tf.keras.layers.Conv1D(filters=128, kernel_size=3, activation='relu'),
        tf.keras.layers.MaxPool1D(pool_size=3),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(256, activation=tf.nn.relu),
        tf.keras.layers.Dense(128, activation=tf.nn.relu),
        tf.keras.layers.Dense(6, activation=tf.nn.softmax)
    ])
"""
model.compile(optimizer=optimizer_fun,
              loss=loss_fun,
              metrics=['accuracy'])

history = model.fit(train_features, train_labels, validation_data=(valid_features, valid_labels), epochs=epochs_num)

predictionResults(model, test_features, test_labels, data_reader)
plotHistory(history)






