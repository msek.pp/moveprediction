import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

def predictionResults(model, test_features, test_labels, data_reader):
    """Predict data labels and show results"""

    classification = model.predict(test_features)

    predict_results = []
    predict_results_int = []
    for row in classification:
        max_output_ind = np.argmax(row)
        predict_results.append(data_reader.intToCategorical(max_output_ind))
        predict_results_int.append(max_output_ind)

    print("\n")
    correct = 0
    for i in range(0, len(predict_results)):
        if predict_results[i] == data_reader.intToCategorical(test_labels[i]):
            correct += 1
        print(f"( Prediction, Reality ): ( {predict_results[i]}, {data_reader.intToCategorical(test_labels[i])} )")

    print(f"\nCorrect predictions: {correct}/{len(test_features)} (", end="")
    print("{:.4f}%)\n".format(correct/len(test_features)))
    model.evaluate(test_features, test_labels)

    confusion_matrix = tf.math.confusion_matrix(test_labels, predict_results_int)
    print("Confusion matrix: ")
    print(confusion_matrix)

def plotHistory(history):
    """Plot loss and accuracy of model over epochs"""

    # Summarize history for accuracy
    plt.figure(1, facecolor='grey')
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('Model accuracy', fontweight='bold')
    plt.ylabel('Accuracy', fontweight='bold')
    plt.xlabel('Epochs', fontweight='bold')
    plt.legend(['Train accuracy', 'Validation accuracy'], loc='upper left')
    plt.grid(True)
    # Summarize history for loss
    plt.figure(2, facecolor='grey')
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss', fontweight='bold')
    plt.ylabel('Loss', fontweight='bold')
    plt.xlabel('Epochs', fontweight='bold')
    plt.legend(['Training loss', 'Validation loss'], loc='upper right')
    plt.grid(True)

    plt.style.use('dark_background')
    plt.show()

