#include <EMGFilters.h>

#define LED 16
#define LED_FREQ 2
#define EMG A0
#define FRAME_LENGTH 70 // 5*10 (uint16_t) + 10 spaces + X, \n, \0 + 7 spare slots
#define DATA_LENGTH 10 // Number of readings from EMG in one frame

EMGFilters filter;

unsigned long time_stamp = 0;
unsigned long time_budget = 0;
unsigned long diode_timer = 0;
static int threshold = 0;

uint8_t data_index = 1;
uint8_t value_counter = 0;
char data_frame[FRAME_LENGTH];

void addValueToFrame(int value);
void startNewFrame();
void sendFrame();
uint16_t readEMG();
void toggleDiode();

void setup()
{
  // Setup LED pin
  pinMode(LED, OUTPUT);
  // Start frame with header
  data_frame[0] = 'X';
  // Init EMG filters
  filter.init(SAMPLE_FREQ_500HZ, NOTCH_FREQ_50HZ, true, true, true);
  // Start serial
  Serial.begin(115200);
  // 1000 us = 1ms, so frequency 1kHz
  time_budget = 1e6 / SAMPLE_FREQ_500HZ;
  // Start diode timer
  diode_timer = millis();
}

// ~~~~~~~~~~~~~~~~~~~ MAIN LOOP ~~~~~~~~~~~~~~~~~~~~~ //

void loop()
{
  // Start counting time
  time_stamp = micros();
  // Acquire data
  uint16_t sensor_data = readEMG();
  // Add data to frame or send data if ready
  if(value_counter >= DATA_LENGTH)
  {
    sendFrame();
    value_counter = 0;
  }
  else
  {
    addValueToFrame(sensor_data);
    ++value_counter;
  }
  // Toggle external LED
  toggleDiode();
  // Count time cost and display
  time_stamp = micros() - time_stamp;

  delayMicroseconds(time_budget - time_stamp);
}

// ~~~~~~~~~~~~~~ FUNCTIONS DEFINITIONS ~~~~~~~~~~~~~~ //

uint16_t readEMG()
{
  int new_value = analogRead(EMG);
  int filtered_data = filter.update(new_value);
  // uint16_t squared_signal = sq(filtered_value);
  uint16_t absolute_signal = (filtered_data < 0) ? -filtered_data : filtered_data;
  return (absolute_signal > threshold) ? absolute_signal : 0;
}

void addValueToFrame(int value)
{
  // Array for converted value
  char int_to_char[10];
  // Now we have new value turned into char
  sprintf(int_to_char, "%d", value);
  // Add space to frame
  data_frame[data_index++] = ' ';
  // Add new value to frame
  int temp_index = 0;
  while(temp_index < 10)
  {
    if(int_to_char[temp_index] == '\0')
      break;

    data_frame[data_index] = int_to_char[temp_index];
    ++data_index;
    ++temp_index;
  }
}

void startNewFrame()
{
  data_index = 1;
}

void sendFrame()
{
  data_frame[data_index] = '\n';
  data_frame[++data_index] = '\0';
  Serial.print(data_frame);

  startNewFrame();
}

void toggleDiode()
{
  uint16_t time = millis() - diode_timer;
  
  if(time >= 1000 / LED_FREQ)
  {
    digitalWrite(LED, !digitalRead(LED));
    diode_timer = millis();
  }
}