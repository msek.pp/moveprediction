# AI sEMG data prediction

Project aims to achieve possibility of prediction human movements, especially hand movements, using machine learning algorithms and sEMG sensor's data.

Project includes the following tasks:
- simple electronic circuit (connect microcontroller, sensor and battery),
- software for microcontroller based sensor system,
- window application implemented using Qt libraries and C++ language,
- AI software for predicting movements (RNN).

Project is developed as a part of "Intelligent Virtualization of Systems and Process Automation" course on Embedded Robotics studies (Wroclaw University of Science and Technology).

## Electronics

For gathering EMG data from muscles, DFRobot SEN0240 was chosen. RP2040 microcontroller on Raspberry Pi Pico W board manage the collected data - read from ADC and send to appliaction. Some LEDs were added to visualize working of
a device.

## Application

Window application is being implemented with Qt libraries and C++ language.

Main functionalities:
- communicating wireless with microcontroller (BT connection)
- viewing sesnor data in real time
- recording data to file
- viewing saved signals
- extract learning samples from signal for neural network
- save samples to a database (json or txt files)

